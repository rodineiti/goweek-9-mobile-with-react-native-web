import React, { useState } from 'react';
import { RouteComponentProps } from "react-router";
import { AsyncStorage, SafeAreaView, View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import api from './../services/api';
interface Props extends RouteComponentProps {}

export default function Book(Props) {

  const { history, match } = Props;
  const id = match.params.id;
  const [date, setDate] = useState('');

  async function handleSubmit() {
    const user_id = await AsyncStorage.getItem('user');
    console.log(user_id)
    await api.post(`/spots/${id}/bookings`, {
      date
    }, {
      headers: { user_id }
    });

    alert('Solicitação de reserva enviado.');
    history.goBack();
  }

  function handleCancel() {
    history.goBack();
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.form}>
      <Text style={styles.label}>DATA DE INTERESSE *</Text>
      <TextInput 
        style={styles.input} 
        placeholder="Qual data você quer reservar?" 
        placeholderTextColor="#999"
        autoCorrect={false}
        value={date}
        onChangeText={setDate} />

        <TouchableOpacity 
          style={styles.button}
          onPress={handleSubmit}>
        <Text style={styles.buttonText}>Solicitar reserva</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={[styles.button, styles.buttonCancel]}
          onPress={handleCancel}>
        <Text style={styles.buttonText}>Cancelar reserva</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    margin:30,
  },
  label: {
    fontWeight: 'bold',
    color:'#444',
    marginBottom: 8,
    marginTop:30,
  },
  input: {
    borderWidth:1,
    borderColor:'#ddd',
    paddingHorizontal: 20,
    fontSize:16,
    color:'#444',
    marginBottom:20,
    borderRadius:2,
  },
  button: {
    height:42,
    backgroundColor:'#f05a5b',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:2,
    marginTop:10,
  },
  buttonCancel: {
    backgroundColor:'#ccc',
  },
  buttonText: {
    color:'#FFF',
    fontWeight:'bold',
    fontSize:16,
  },
});