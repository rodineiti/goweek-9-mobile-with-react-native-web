import React, { useState, useEffect } from 'react';
import  socketio from 'socket.io-client';
import { RouteComponentProps } from "react-router";
import { AsyncStorage, Image, SafeAreaView, ScrollView, Text, StyleSheet } from 'react-native';

import SpotList from './../components/SpotList';
interface Props extends RouteComponentProps {}

export default function List(Props) {

  const { history } = Props;
  const [techs, setTechs] = useState([]);

  useEffect(() => {
    AsyncStorage.getItem('user').then(user_id => {
      const socket = socketio('http://localhost:3333', {
        query: { user_id }
      });
      socket.on('booking_response', booking => {
        alert(`Sua reserva em ${booking.spot.company} em ${booking.date} foi ${booking.approved ? 'APROVADA' : 'REJEITADA'}`);
      });
    })
  }, []);

  useEffect(() => {
    AsyncStorage.getItem('techs').then(item => {
      const techsArray = item.split(',').map(tech => tech.trim());
      setTechs(techsArray);
    })
  }, []);
  
  return (
    <SafeAreaView style={styles.container}>
      <Image style={styles.logo} source={{uri:'http://localhost:3333/files/erro_api_0003-1570029740153.png'}} />
      <ScrollView>
      {techs.map(tech => <SpotList key={tech} tech={tech} history={history} />)}
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex:1,
  },
  logo: {
    width:200,
    height:100,
    resizeMode:'contain',
    alignSelf:'center',
  },
  button: {
    height:42,
    backgroundColor:'#f05a5b',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:2
  },
  buttonText: {
    color:'#FFF',
    fontWeight:'bold',
    fontSize:16,
  },
});