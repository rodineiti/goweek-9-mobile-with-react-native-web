import React from 'react';
import { NativeRouter as Router, Route, Switch } from "react-router-native";

import Login from './pages/Login';
import List from './pages/List';
import Book from './pages/Book';

export default function Routes() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/list" component={List} />
          <Route exact path="/book/:id" component={Book} />
        </Switch>
      </Router>
    )
};
